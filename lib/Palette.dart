import 'package:flutter/material.dart';

class Palette {
  static const Color colorGreen = Color.fromRGBO(35, 152, 138, 1);
  static const Color colorLightGreen = Color.fromRGBO(218, 246, 244, 1);
  static const Color colorGreyGreen = Color.fromRGBO(194, 231, 228, 1);
  static const Color colorMediumGreen = Color.fromRGBO(4, 213, 190, 1);
  static const Color colorOrange = Color.fromRGBO(215, 95, 17, 1);
  static const Color colorLightOrange = Color.fromRGBO(255, 240, 231, 1);
  static const Color colorGreyOrange = Color.fromRGBO(247, 222, 208, 1);
  static const Color colorMediumOrange = Color.fromRGBO(248, 103, 15, 1);
  static const Color colorGrey = Color.fromRGBO(232, 235, 240, 1.0);
}
