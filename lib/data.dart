import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

import 'Palette.dart';
import 'widgets/bottom_bar.dart';
import 'widgets/fab.dart';
import 'widgets/graph.dart';
import 'widgets/progress_card.dart';

class Data extends StatefulWidget {
  @override
  _DataState createState() => _DataState();
}

class _DataState extends State<Data> {
  List labels = [];
  List<String> weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  List<String> months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  List<int> monthsDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  String dropDownValue = 'This week';
  int month;
  List spaceCorrector = [8, 6];

  String taskStatsActive;

  int barActive;

  List get monthDays => new List<int>.generate(monthsDays[month], (i) => i + 1);

  int get columns => labels.length * 2;

  double get barWidth => ((MediaQuery.of(context).size.width - 52) / columns) + spaceCorrector[0];

  @override
  void initState() {
    super.initState();
    labels = weekDays;
    month = DateTime.now().month - 1;
    barActive = Random().nextInt(labels.length) - 1;
    taskStatsActive = 'w';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Fab(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(isActive4: true),
      body: buildBody(),
    );
  }

  Widget buildBody() {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: AlignmentDirectional.centerStart,
            end: AlignmentDirectional.centerEnd,
            colors: [
              Color.fromRGBO(0, 212, 192, 1),
              Color.fromRGBO(0, 184, 189, 1),
            ],
          ),
        ),
        child: Column(
          children: <Widget>[
            buildTitle(),
            buildSubTitle(),
            buildBars(),
            Expanded(
              child: ListView(
                children: <Widget>[
                  buildBottomCard(),
                  buildGraph(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// @todo Select date range
  Widget buildTitle() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Text(
              'Data',
              style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              new DropdownButton<String>(
                hint: Text(
                  dropDownValue,
                  style: TextStyle(color: Colors.white),
                ),
                items: <String>['This week', 'This month', 'This year'].map((String value) {
                  return new DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value, style: TextStyle(color: Colors.black, fontSize: 16)),
                  );
                }).toList(),
                style: TextStyle(color: Colors.black, fontSize: 16),
                onChanged: (value) {
                  print(value);
                  List l = weekDays;
                  spaceCorrector = [8, 6];
                  String t = 'w';
                  switch (value) {
                    case 'This month':
                      l = monthDays;
                      spaceCorrector = [0, 0];
                      t = 'm';
                      break;
                    case 'This year':
                      l = months;
                      spaceCorrector = [10, 10];
                      t = 'y';
                      break;
                  }
                  setState(() {
                    barActive = Random().nextInt(l.length) - 1;
                    labels = l;
                    dropDownValue = value;
                    taskStatsActive = t;
                  });
                },
                iconEnabledColor: Colors.white,
                isDense: true,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildSubTitle() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      alignment: AlignmentDirectional.centerStart,
      child: Text(
        'The most efficient on Monday',
        style: TextStyle(
          color: Colors.white,
          fontSize: 18,
        ),
      ),
    );
  }

  Widget buildBars() {
    return Container(
      padding: const EdgeInsets.all(16),
      height: 180,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[...buildGraphBars()],
            ),
          ),
          Divider(indent: 6, height: 1, endIndent: 6, color: Colors.white),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[...buildBarTags()],
          )
        ],
      ),
    );
  }

  Widget buildBar({double percent: 0, bool active: false}) {
    return Align(
      alignment: AlignmentDirectional.bottomCenter,
      child: Container(
        width: 20,
        height: 125 * percent,
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2),
          gradient: LinearGradient(
            end: AlignmentDirectional.bottomCenter,
            begin: AlignmentDirectional.topCenter,
            colors: <Color>[
              active ? Colors.white : Color.fromRGBO(84, 223, 212, 1),
              Color.fromRGBO(30, 216, 198, 1),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildBottomCard() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            children: <Widget>[
              ProgressCard(
                color: Palette.colorGreen,
                lightColor: Palette.colorLightGreen,
                title: 'Mission progress',
                subtitle: '26 completed tasks',
                percent: Random().nextInt(99),
                icon: Icons.assignment,
              ),
              SizedBox(height: 16),
              ProgressCard(
                color: Palette.colorOrange,
                lightColor: Palette.colorLightOrange,
                title: 'Completed goal',
                subtitle: '3 completed goals',
                percent: Random().nextInt(50),
                icon: Icons.check_circle,
              ),
              SizedBox(height: 16),
            ],
          ),
        ),
      ),
    );
  }

  List buildBarTags() {
    var tags = [];
    var i = 0;
    for (var label in labels) {
      tags.add(
        Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(vertical: 4.0),
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 0.0),
            decoration: BoxDecoration(
              color: (barActive == i) ? Colors.white : Colors.transparent,
              borderRadius: BorderRadius.circular(4),
            ),
            child: Text(label.toString(),
                style: TextStyle(
                  color: (barActive == i) ? Palette.colorOrange : Colors.white,
                  fontSize: 12,
                  fontWeight: (barActive == i) ? FontWeight.bold : FontWeight.normal,
                )),
          ),
          width: barWidth,
        ),
      );
      i++;
    }
    return tags;
  }

  List buildGraphBars() {
    var tags = [];
    var i = 0;
    for (var label in labels) {
      tags.add(
        Container(
          alignment: Alignment.center,
          child: buildBar(active: (barActive == i), percent: Random().nextDouble()),
          width: barWidth,
        ),
      );
      i++;
    }

    return tags;
  }

  Widget buildGraph() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      height: 250,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  'Task stadistics',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      taskStatsActive = 'w';
                    });
                  },
                  child: Text(
                    'Week',
                    style: TextStyle(
                      color: taskStatsActive == 'w' ? Colors.black : Colors.black38,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      taskStatsActive = 'm';
                    });
                  },
                  child: Text(
                    'Month',
                    style: TextStyle(
                      color: taskStatsActive == 'm' ? Colors.black : Colors.black38,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      taskStatsActive = 'y';
                    });
                  },
                  child: Text(
                    'Year',
                    style: TextStyle(
                      color: taskStatsActive == 'y' ? Colors.black : Colors.black38,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Expanded(
            child: Container(
              child: Graph(
                color: Palette.colorGreen,
                lightColor: Palette.colorLightGreen,
                data: [8, 14, 0, 6, 7, 5, 9],
                range: taskStatsActive,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
