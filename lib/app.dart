import 'package:flutter/material.dart';
import 'package:habit/Palette.dart';
import 'package:habit/widgets/bottom_bar.dart';
import 'package:habit/widgets/fab.dart';
import 'package:habit/widgets/habit_card.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: Fab(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomBar(
          isActive3: true,
        ),
        body: buildBody());
  }

  Widget buildBody() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[Colors.white, Palette.colorGrey],
          end: AlignmentDirectional.bottomCenter,
        ),
      ),
      child: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: buildTitle(),
            backgroundColor: Colors.white,
            floating: true,
            pinned: true,
            titleSpacing: 0,
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                buildLongTermHabit(),
                SizedBox(height: 16),
                buildShortTermHabit(),
                SizedBox(height: 16),
                buildHabitsGrid(),
                SizedBox(height: 16),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildTitle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: Text(
            'Habit',
            style: TextStyle(color: Colors.black, fontSize: 28, fontWeight: FontWeight.bold),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.calendar_today,
              color: Colors.black,
              size: 16,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 4.0),
              child: Text(
                DateTime.now().year.toString(),
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildLongTermHabit() {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(color: Palette.colorLightGreen, borderRadius: BorderRadius.circular(8)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Text(
              'Long-term habit',
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Palette.colorGreen, fontSize: 16, fontFamily: 'Poppins'),
            ),
          ),
          buildPercentText(percent: 70, color: Palette.colorGreyGreen),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 12),
            child: Text(
              '7/10',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Palette.colorGreen,
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildHabitsGrid() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            new HabitCard(
              title: 'Weekly basketball',
              dates: '19/1-20/1',
              days: ['Wed', 'Thu'],
              percent: 60,
              color: Palette.colorGreen,
              lightColor: Palette.colorLightGreen,
              mediumColor: Palette.colorMediumGreen,
            ),
            new SizedBox(width: 16),
            new HabitCard(
              title: 'Improve Design professional depth',
              dates: '1/24~1/28',
              days: ['Fri'],
              percent: 40,
              color: Palette.colorOrange,
              lightColor: Palette.colorLightOrange,
              mediumColor: Palette.colorMediumOrange,
            ),
          ],
        ),
        new SizedBox(height: 16),
        Row(
          children: <Widget>[
            new HabitCard(
              title: 'Design exercise and design thinking',
              dates: '19/6-19/12',
              days: ['Mon', 'Wed'],
              percent: 30,
              color: Palette.colorGreen,
              lightColor: Palette.colorLightGreen,
              mediumColor: Palette.colorMediumGreen,
            ),
            new SizedBox(width: 16),
            new HabitCard(
              title: 'Reading Notes',
              dates: '19/2-19/4',
              days: ['Sat', 'Sun'],
              percent: 10,
              color: Palette.colorGreen,
              lightColor: Palette.colorLightGreen,
              mediumColor: Palette.colorMediumGreen,
            ),
          ],
        ),
      ],
    );
  }

  Widget buildShortTermHabit() {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(color: Palette.colorLightOrange, borderRadius: BorderRadius.circular(8)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Text(
              'Short-term habit',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Palette.colorOrange,
                fontSize: 16,
              ),
            ),
          ),
          buildPercentText(percent: 90, color: Palette.colorGreyOrange),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 12),
            child: Text(
              '7/8',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Palette.colorOrange,
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildPercentText({int percent, Color color}) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: percent.toString(),
            style: TextStyle(fontWeight: FontWeight.bold, color: color, fontSize: 40),
          ),
          TextSpan(
            text: '%',
            style: TextStyle(fontWeight: FontWeight.bold, color: color, fontSize: 32),
          ),
        ],
      ),
    );
  }
}
