import 'package:flutter/material.dart';
import 'package:habit/Palette.dart';

import '../app.dart';
import '../data.dart';

class BottomBar extends StatelessWidget {
  final bool isActive1;
  final bool isActive2;
  final bool isActive3;
  final bool isActive4;

  const BottomBar({
    Key key,
    this.isActive1: false,
    this.isActive2: false,
    this.isActive3: false,
    this.isActive4: false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color active = Palette.colorGreen;
    Color unactivated = Colors.black38;
    return Container(
      decoration: BoxDecoration(border: Border(top: BorderSide(width: 1.0, color: Palette.colorGrey))),
      height: 50,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          GestureDetector(
            child: Icon(Icons.access_alarm, color: isActive1 ? active : unactivated),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (context) => App(),
                ),
              );
            },
          ),
          GestureDetector(
            child: Icon(Icons.calendar_today, color: isActive2 ? active : unactivated),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (context) => App(),
                ),
              );
            },
          ),
          GestureDetector(
            child: Icon(Icons.star, color: isActive3 ? active : unactivated),
            onTap: () {
              if (isActive3 == false) {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => App(),
                  ),
                );
              }
            },
          ),
          GestureDetector(
            child: Icon(Icons.filter, color: isActive4 ? active : unactivated),
            onTap: () {
              if (isActive4 == false) {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => Data(),
                  ),
                );
              }
            },
          ),
        ],
      ),
    );
  }
}
