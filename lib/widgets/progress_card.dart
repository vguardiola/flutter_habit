import 'package:flutter/material.dart';

import 'arc_progress_bar.dart';

class ProgressCard extends StatelessWidget {
  final String title;
  final String subtitle;
  final int percent;
  final Color color;
  final Color lightColor;
  final IconData icon;
  ProgressCard({
    Key key,
    this.title: '',
    this.subtitle: '',
    this.percent,
    this.color: Colors.red,
    this.lightColor: Colors.redAccent,
    this.icon: Icons.access_alarm,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
            color: lightColor,
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: Icon(
            icon,
            color: color,
            size: 32,
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                    color: Colors.black,
                  ),
                ),
                Text(
                  subtitle,
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 12,
                    color: Colors.black38,
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          child: ArcProgressBar(
            backgroundColor: Colors.white,
            color: color,
            lightColor: lightColor,
            percent: percent,
          ),
          width: 40,
          height: 40,
        ),
      ],
    );
  }
}
