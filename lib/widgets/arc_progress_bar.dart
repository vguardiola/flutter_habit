import 'dart:math';

import 'package:flutter/material.dart';

/// Insppired by: https://jorgecastillo.dev/flutter-canvas-i-wrap-fab-in-custom-progress-loader

class ArcProgressBar extends StatelessWidget {
  final Color backgroundColor;
  final int percent;
  final Color color;
  final Color lightColor;

  const ArcProgressBar({
    Key key,
    this.backgroundColor: Colors.white,
    this.percent: 50,
    this.color: Colors.green,
    this.lightColor: Colors.green,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(8),
      height: 4,
      decoration: BoxDecoration(
        color: this.backgroundColor,
        borderRadius: BorderRadius.circular(2),
      ),
      child: CustomPaint(
        painter: ArcProgressPaint(
          color: color,
          lightColor: lightColor,
          percent: percent,
        ),
        child: Center(
          child: Text(
            '$percent%',
            style: TextStyle(
              fontSize: 10,
              color: color,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}

class ArcProgressPaint extends CustomPainter {
  final Paint _paintBackground = new Paint();
  final Paint _paintTransparent = new Paint();
  final int percent;
  double strokeWidth = 6;

  ArcProgressPaint({color, this.percent, Color lightColor}) {
    _paintBackground.color = lightColor;
    _paintBackground.style = PaintingStyle.stroke;
    _paintBackground.strokeWidth = strokeWidth;
    _paintTransparent.color = color;
    _paintTransparent.style = PaintingStyle.stroke;
    _paintTransparent.strokeWidth = strokeWidth;
    _paintTransparent.strokeCap = StrokeCap.square;
  }

  @override
  void paint(Canvas canvas, Size size) {
    double radius = strokeWidth * 2 + size.height / 2;
    canvas.drawCircle(Offset(size.height / 2, size.height / 2), radius, _paintBackground);
    canvas.drawArc(
      Rect.fromCircle(center: Offset(size.height / 2, size.height / 2), radius: radius),
      -pi / 2,
      calcAngle(percent),
      false,
      _paintTransparent,
    );
  }

  double calcAngle(percent) {
    return (percent * -pi) / 50;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
