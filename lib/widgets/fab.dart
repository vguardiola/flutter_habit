import 'package:flutter/material.dart';

import '../data.dart';

class Fab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            new MaterialPageRoute(
              builder: (context) => Data(),
            ));
      },
      child: Container(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          gradient: LinearGradient(
            colors: <Color>[
              Color.fromRGBO(0, 210, 192, 1.0),
              Color.fromRGBO(0, 189, 190, 1.0),
            ],
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 8,
              spreadRadius: 2,
            ),
          ],
        ),
      ),
    );
  }
}
