import 'package:flutter/material.dart';

class ProgressBar extends StatelessWidget {
  final Color backgroundColor;
  final double percent;
  final Color color;

  const ProgressBar({
    Key key,
    this.backgroundColor: Colors.grey,
    this.percent: 0,
    this.color: Colors.green,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(8),
      height: 4,
      decoration: BoxDecoration(
        color: this.backgroundColor,
        borderRadius: BorderRadius.circular(2),
      ),
      child: CustomPaint(
        painter: ProgressPaint(color: color, width: percent, height: 4),
      ),
    );
  }
}

class ProgressPaint extends CustomPainter {
  final Paint _paintBackground = new Paint();
  final Paint _paintTransparent = new Paint();
  final double width;
  final double height;

  ProgressPaint({color, this.width, this.height}) {
    _paintBackground.color = color;
    _paintBackground.style = PaintingStyle.fill;
    _paintTransparent.color = Colors.transparent;
  }

  @override
  void paint(Canvas canvas, Size size) {
    if (width > 0) {
      var midHeight = height / 2;
      var midMinusHeight = (height / -1) - 2;
      canvas.drawCircle(Offset(midMinusHeight, midMinusHeight), midHeight, _paintBackground);
      final start = Offset(midMinusHeight, midMinusHeight);
      final end = Offset(size.width * (width / 100), midMinusHeight);
      _paintBackground.style = PaintingStyle.stroke;
      _paintBackground.strokeWidth = height;
      canvas.drawLine(start, end, _paintBackground);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
