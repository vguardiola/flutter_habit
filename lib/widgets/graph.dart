import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:habit/Palette.dart';

class Graph extends StatelessWidget {
  final Color backgroundColor;
  final Color color;
  final Color lightColor;
  final List<int> data;
  final String range;

  const Graph({
    Key key,
    this.backgroundColor: Colors.white,
    this.color: Colors.green,
    this.lightColor: Colors.green,
    this.data,
    this.range,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(8),
      height: 4,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Container(
              height: 150,
              decoration: BoxDecoration(
                color: this.backgroundColor,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.white,
                    lightColor,
                  ],
                ),
              ),
              child: CustomPaint(
                size: Size(MediaQuery.of(context).size.width, 150),
                painter: GraphPaint(
                  color: color,
                  lightColor: lightColor,
                  data: data,
                ),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(child: Text('Sun', style: TextStyle(color: Colors.black38, fontSize: 12))),
              SizedBox(child: Text('Mon', style: TextStyle(color: Colors.black, fontSize: 12))),
              SizedBox(child: Text('Tue', style: TextStyle(color: Colors.black38, fontSize: 12))),
              SizedBox(child: Text('Wed', style: TextStyle(color: Colors.black38, fontSize: 12))),
              SizedBox(child: Text('Thu', style: TextStyle(color: Colors.black38, fontSize: 12))),
              SizedBox(child: Text('Fri', style: TextStyle(color: Colors.black38, fontSize: 12))),
              SizedBox(child: Text('Sat', style: TextStyle(color: Colors.black38, fontSize: 12))),
            ],
          ),
        ],
      ),
    );
  }
}

class GraphPaint extends CustomPainter {
  final int percent;
  double strokeWidth = 6;
  Color color;
  Color lightColor;
  double barWidth;
  List<int> data;

  GraphPaint({this.color, this.percent, this.lightColor, this.data});

  @override
  void paint(Canvas canvas, Size size) {
    Paint linePaint = new Paint();
    linePaint.strokeWidth = 2;
    linePaint.style = PaintingStyle.stroke;
    linePaint.color = Palette.colorGreen;
    Paint pathPaint = new Paint();
    pathPaint.strokeWidth = 2;
    pathPaint.style = PaintingStyle.fill;
    pathPaint.color = Colors.white;
    Paint dotPaint = new Paint();
    dotPaint.strokeWidth = 2;
    dotPaint.style = PaintingStyle.fill;
    dotPaint.color = Palette.colorGreen;
    int i = 0;
    int maxValue = data.reduce(max);
    barWidth = size.width / data.length;
    Path pathBg = Path();
    Path path = Path();
    double x, y = 0;
    for (var dot in data) {
      x = (i == 0) ? 0 : (i * barWidth) + 24;
      y = (maxValue - dot) * size.height / maxValue;
      if (i == data.length - 1) {
        x = size.width;
      }
      if (i == 0) {
        pathBg.moveTo(x, size.height);
        pathBg.lineTo(x, size.height);
        pathBg.lineTo(0, size.height);
        pathBg.lineTo(0, 0);
        pathBg.moveTo(x, size.height);
        pathBg.lineTo(x - 2, y - 2);
        path.moveTo(x, y);
      } else {
        pathBg.lineTo(x, y);
        path.lineTo(x, y);
      }
      i++;
    }
    pathBg.lineTo(x, size.height);
    pathBg.lineTo(size.width, size.height);
    pathBg.lineTo(size.width, 0);
    pathBg.lineTo(0, 0);
    canvas.drawPath(pathBg, pathPaint);
    canvas.drawPath(path, linePaint);
    i = 0;
    for (var dot in data) {
      x = (i == 0) ? 0 : (i * barWidth) + 24;
      y = (maxValue - dot) * size.height / maxValue;
      if (i == data.length - 1) {
        x = size.width;
      }
      Offset textOffset = Offset(x - 4, y - 20);
      canvas.drawCircle(Offset(x, y), 3, dotPaint);
      final TextPainter textPainter = TextPainter(
        text: TextSpan(
            text: dot.toString(),
            style: TextStyle(
              color: Palette.colorGreen,
              fontSize: 12,
            )),
        textAlign: TextAlign.justify,
        textDirection: TextDirection.ltr,
      )..layout(maxWidth: size.width - 24.0);
      textPainter.paint(canvas, textOffset);
      i++;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
