import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:habit/widgets/progress_bar.dart';

class HabitCard extends StatelessWidget {
  String title;
  String dates;
  List<String> days;
  int percent;
  Color color;
  Color lightColor;
  Color mediumColor;
  HabitCard({
    this.title: '',
    this.dates: '',
    this.days,
    this.percent: 0,
    this.color: Colors.red,
    this.lightColor: Colors.red,
    this.mediumColor: Colors.red,
  });
  Color _colorGrey = Color.fromRGBO(232, 235, 240, 1.0);
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        height: 175,
        decoration: BoxDecoration(
          borderRadius: BorderRadiusDirectional.circular(12),
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              buildDot(mediumColor),
              buildCardTitle(title),
              buildCardDates(dates),
              Expanded(
                flex: 2,
                child: Container(),
              ),
              buildCardWeekDaysChips(days ?? [], color, lightColor),
              SizedBox(
                height: 10,
              ),
              ProgressBar(
                percent: percent.toDouble(),
                color: mediumColor,
                backgroundColor: _colorGrey,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildDot(Color color) {
    return Container(
      width: 12,
      height: 12,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: color,
      ),
    );
  }

  Widget buildCardTitle(String title) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Text(
        title,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w500,
        ),
        maxLines: 2,
        softWrap: true,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget buildCardDates(String dates) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Text(
        dates,
        style: TextStyle(color: Colors.grey, fontSize: 12),
      ),
    );
  }

  Widget buildCardWeekDaysChips(List<String> days, Color color, Color lightColor) {
    return Container(
      width: double.infinity,
      height: 28,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          for (var day in days)
            Padding(
              padding: const EdgeInsets.all(3),
              child: Container(
                child: Text(
                  day,
                  style: TextStyle(color: color, fontSize: 12),
                ),
                decoration: BoxDecoration(
                  color: lightColor,
                  borderRadius: BorderRadius.circular(12),
                ),
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              ),
            )
        ],
      ),
    );
  }
}
